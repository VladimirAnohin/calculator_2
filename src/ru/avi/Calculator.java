package ru.avi;


/**
 * Вспомогательный класс калькулятор.
 * Содержит методы арифметических вычислений (сложение, вычитание, умножение, деление, выделение остатка, возведение в степень)
 *
 * @author Анохин Владимир, 13ОИТ18к
 */
public class Calculator {
    /**
     * Метод выполняет сложение двух чисел
     *
     * @param a
     * @param b
     * @return сумму
     */
    public static double addition(double a, double b) {
        return a + b;
    }

    /**
     * Метод выполняет вычитание двух чисел
     *
     * @param a
     * @param b
     * @return разность
     */
    public static double subtraction(double a, double b) {
        return a - b;
    }

    /**
     * Метод выполняет умножение двух чисел
     *
     * @param a
     * @param b
     * @return произведение
     */
    public static double multiplication(double a, double b) {
        return a * b;
    }

    /**
     * Метод выполняет возведение в степень
     *
     * @param a
     * @param b
     * @return степень числа
     */
    public static double pow(double a, double b) {
        if (b == 0) return 1;
        if (b == 1) return a;
        if (b > 1) return pow_1(a, b);
        if (b < 0) {
            double c = b * -1;
            return 1 / pow_1(a, c);
        } else {
            return 0;
        }
    }

    /**
     * Вспомогательный метод возведения в степень. Содержит цикл, возводящий число в указанную степень.
     *
     * @param a
     * @param b
     * @return степень числа
     */
    public static double pow_1(double a, double b) {
        double temp = 0, res = a;
        for (int i = 1; i < b; i++) {
            temp = res * a;
            res = temp;

        }
        return temp;
    }

    /**
     * Метод выполняет деление двух вещественных чисел
     *
     * @param a
     * @param b
     * @return частное
     */
    public static double division(double a, double b) {
        if (b == 0) throw new ArithmeticException("Деление на ноль");
        return a / b;
    }

    /**
     * Метод выполняет деление двух целых чисел
     *
     * @param a
     * @param b
     * @return частное
     */

    public static int division(int a, int b) {
        if (b == 0) throw new ArithmeticException("Деление на ноль");
        return a / b;
    }

    /**
     * Метод выполняет деление с остатком
     *
     * @param a
     * @param b
     * @return остаток
     */
    public static double modulo(double a, double b) {
        return a % b;
    }
}
