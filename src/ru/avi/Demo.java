package ru.avi;

import java.io.*;
import java.util.Scanner;
import java.util.regex.*;


/**
 * Демонстрационный класс
 * @author Анохин Владимир. 13ОИТ18к
 */
public class Demo {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        System.out.print("Введите данные: ");
        String volume = sc.nextLine();
        Pattern regular = Pattern.compile("^[+-]?\\d+(\\.\\d+)?\\s?[+-]?\\d+(\\.\\d+)?\\s?[+-/*^%]$");
        Matcher m = regular.matcher(volume);
        boolean b = m.matches();

        while (b == false) {
            System.out.println("Некорректный формат ввода.\nПовторите ввод: ");
            volume = sc.nextLine();
            regular = Pattern.compile("^[+-]?\\d+(\\.\\d+)?\\s?[+-]?\\d+(\\.\\d+)?\\s?[+-/*^%]$");
            m = regular.matcher(volume);
            b = m.matches();
        }

        String[] tmp = volume.split(" ");
        double u = Double.parseDouble(tmp[0]), y = Double.parseDouble(tmp[1]);
        String sign = tmp[2];

        System.out.println(Helper.select(u, y, sign));

        BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"));
        String string = bufferedReader.readLine();

        tmp = string.split(" ");
        u = Double.parseDouble(tmp[0]);
        y = Double.parseDouble(tmp[1]);
        sign = tmp[2];

        Helper.write(Helper.select(u, y, sign));
    }
}
