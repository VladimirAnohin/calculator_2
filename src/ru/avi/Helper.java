package ru.avi;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Вспомогательный класс
 * Содержит 2 метода. Метод записи данных в файл, метод выбора арифмитической операции в зависимости от введенного значения.
 * @author Анохин Владимир. 13ОИТ18к.
 */
public class Helper {
    /**
     * Метод выполняет запись результата в файл output.txt
     * @param res
     */
    public static void write(double res) throws IOException {
        File file1 = new File("output.txt");

        if (!file1.exists()) {
            file1.createNewFile();
        }
        BufferedWriter dataOutputStream = new BufferedWriter(new FileWriter("output.txt"));

        String res_string = Double.toString(res);
        dataOutputStream.write(res_string);
        dataOutputStream.close();
    }

    /**
     * Метод выполняет арифметическое вычисление двух операндов в зависимости от полученного арифмитического знака
     *
     * @param u
     * @param y
     * @param sign
     * @return значение арифмитической операции
     * @throws IOException
     */
    public static double select(double u, double y, String sign) throws IOException {
        switch (sign) {
            case "+":
                double res_add = Calculator.addition(u, y);
                write(res_add);
                return res_add;
            case "-":
                double res_sub = Calculator.subtraction(u, y);
                write(res_sub);
                return Calculator.subtraction(u, y);
            case "/":
                double res_div = Calculator.division(u, y);
                write(res_div);
                return Calculator.division(u, y);
            case "*":
                double res_multi = Calculator.multiplication(u, y);
                write(res_multi);
                return Calculator.multiplication(u, y);
            case "^":
                double res_pow = Calculator.pow(u, y);
                write(res_pow);
                return Calculator.pow(u, y);
            case "%":
                double res_modulo = Calculator.modulo(u, y);
                write(res_modulo);
                return Calculator.modulo(u, y);
        }
        return 0;
    }
}
